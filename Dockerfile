FROM alpine:3.12

ARG SATIS_VERSION=1.0.0

# Install composer/satis
RUN apk add --no-cache \
    composer \
    # need to get nanoseconds
    coreutils \
    curl \
    git \
    logrotate \
    mercurial \
    nginx \
    nginx-mod-http-headers-more \
    openssh \
    openssl \
    php7 \
    subversion \
    supervisor \
    tzdata \
    unzip \
    zip \
  && composer self-update \
  # Install composer/satis
  && composer require \
    --classmap-authoritative \
    --no-progress \
    --no-suggest \
    --optimize-autoloader \
    --prefer-dist \
    --prefer-stable \
    --sort-packages \
    --working-dir=/root \
    "composer/satis:${SATIS_VERSION}" \
  && rm -rf /root/.composer /root/.subversion \
  # Configure cron
  && mkdir -p /var/log/cron \
  && sed -i "2 a *\t*\t*\t*\t*\trun-parts /etc/periodic/minutely" /etc/crontabs/root \
  # Configure satis
  && mkdir -p /composer \
  # Configure php
  && mkdir -p /var/log/php7 \
  && sed -i -e "s|^memory_limit = .*|memory_limit = -1|" \
    -e "s|^;error_log = php.*|error_log = /var/log/php7/php_errors.log|" \
    -e "s|^log_errors_max_len = .*|log_errors_max_len = 0|" /etc/php7/php.ini \
  # Configure nginx
  && mkdir -p /var/run/nginx

COPY satis/ /root/
COPY cron/ /etc/periodic/minutely/
COPY logrotate/ /etc/logrotate.d/
COPY nginx/ /etc/nginx/conf.d/
COPY supervisor/ /etc/supervisor.d/

ENV COMPOSER_HOME=/composer
ENV PATH=/root/vendor/bin:$PATH

COPY docker-entrypoint.sh /
RUN chmod a+x /docker-entrypoint.sh \
  && chmod a+x /etc/periodic/minutely/*
EXPOSE 80/tcp
WORKDIR /composer

ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["supervisord", "-c", "/etc/supervisord.conf", "-n"]
