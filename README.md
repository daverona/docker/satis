# daverona/satis

[![pipeline status](https://gitlab.com/daverona/docker/satis/badges/master/pipeline.svg)](https://gitlab.com/daverona/docker/satis/-/commits/master)

This is a repository for Docker images of [satis](https://getcomposer.org/doc/articles/handling-private-packages-with-satis.md#satis).

* GitLab repository: [https://gitlab.com/daverona/docker/satis](https://gitlab.com/daverona/docker/satis)
* Docker registry: [https://hub.docker.com/r/daverona/satis](https://hub.docker.com/r/daverona/satis)
* Available releases: [https://gitlab.com/daverona/docker/satis/-/releases](https://gitlab.com/daverona/docker/satis/-/releases)

## Quick Start

```bash
docker container run --rm \
  --publish 80:80 \
  --volume "${PWD}/composer:/composer" \
  --volume "${PWD}/data:/var/lib/nginx/html" \
  daverona/satis
```

## Usage

```bash
docker container run --rm \
  --volume "${PWD}/composer:/composer" \
  --volume "${PWD}/data:/var/lib/nginx/html" \
  daverona/satis \
    satis --ansi -vvv build /composer/satis.json /var/lib/nginx/html
```

## References

* satis: [https://getcomposer.org/doc/articles/handling-private-packages-with-satis.md#satis](https://getcomposer.org/doc/articles/handling-private-packages-with-satis.md#satis)
* composer/satis: [https://github.com/composer/satis](https://github.com/composer/satis)
* lukaszlach/satis-server: [https://github.com/lukaszlach/satis-server](https://github.com/lukaszlach/satis-server)
* adnanh/webhook: [https://github.com/adnanh/webhook](https://github.com/adnanh/webhook)
