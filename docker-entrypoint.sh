#!/bin/ash
set -e

[ ! -z "$TZ" ] && sed -i "s|^;date.timezone =.*$|date.timezone = $TZ|" /etc/php7/php.ini

if [ "supervisord" == "$(basename $1)" ]; then
  # If /app/app/config/parameters.yml is missing, make one with default values.
  if [ ! -f /composer/satis.json ]; then
    echo
    echo "CANNOT FIND /composer/satis AND IT WILL BE CREATED USING DEFAULT VALUES."
    echo
    cp /root/satis.json.dist /composer/satis.json
  fi
fi

exec "$@"
